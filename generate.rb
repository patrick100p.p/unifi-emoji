#!/usr/bin/ruby

emoji = '🍆'
emoji = ARGV[0] if ARGV[0]

sets = []

emoji.each_codepoint do |em|
  if em < 0x10000
    puts [em].pack('U') + '  is not 4 byte, should be fine as-is'
    next
  end
  bytes = (em - 0x10000)
  unpack = bytes.to_s(2).rjust(20, '0').chars.each_slice(10).to_a
  unpack = unpack.map(&:join).map { |bin| bin.to_i(2) }
  sets << {
    wrong: [(unpack[0] + 0xD800)].pack('U') + [(unpack[1] + 0xDC00)].pack('U'),
    right: [em].pack('U')
  }
end

File.open('.fixemoji', 'w') do |f|
  f.puts("#!/bin/sh\nfix_emoji() {")
  sets.each do |i|
    puts 'adding ' + i[:right] + '  to file'
    l = "\tLC_ALL=C sed 's/" + i[:wrong] + '/' + i[:right] + '/g\' $1 > '
    l += 'tmp.emoji; mv tmp.emoji $1;'
    f.puts(l)
  end
  f.puts('}')
  f.close
  puts 'done!'
end
